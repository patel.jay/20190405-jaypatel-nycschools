//
//  SchoolTableViewCell.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import UIKit

/// Custom `UITableViewCell` to display list of NYC Schools
class SchoolTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    func configure(school: School) {
        schoolNameLabel.text = school.schoolName
        overviewLabel.text = school.overview
        cityLabel.text = school.city
    }
    
}
