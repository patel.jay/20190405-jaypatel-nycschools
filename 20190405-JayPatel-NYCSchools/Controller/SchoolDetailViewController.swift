//
//  SchoolDetailViewController.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import UIKit
import MessageUI
import SafariServices
import MapKit

class SchoolDetailViewController: UIViewController {

    //MARK:- School Data Outlets
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var studentsTotalLabel: UILabel!
    @IBOutlet var SATLabels: [UILabel]!
    @IBOutlet weak var sportsLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var extraCurrActivityLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var hoursImageView: UIImageView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailImageView: UIImageView!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var websiteImageView: UIImageView!
    @IBOutlet weak var mapLabel: UILabel!
    @IBOutlet weak var mapImageView: UIImageView!
    
    //MARK:- Tap Gesture Outlets
    @IBOutlet var tapGestureAddableImages: [UIImageView]!
    @IBOutlet var tapGestureAddableLabels: [UILabel]!
    
    //MARK:- SAT Outlets
    @IBOutlet weak var SATTakersLabel: UILabel!
    @IBOutlet weak var scoreTitleLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    
    var school: School!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        assert(school != nil, "School details not provided. Please pass data for school to be displayed")
        
        updateSATLabels(result: nil)
        updateUI()
        downloadSATDetail()
        addTapGesture()
        
    }
    
    // MARK:- UI Updates
    
    /// Update outlets and UI based on `School` object
    func updateUI()  {
        schoolLabel.text = school.schoolName
        studentsTotalLabel.text = "Total Students\n\(school.totalStudents)"
        overviewLabel.text = school.overview
        hoursLabel.text = school.hours
        phoneLabel.text = school.phone
        emailLabel.text = school.email ?? "Unavailable"
        websiteLabel.text = school.websiteString
        mapLabel.text = school.location ?? "Unavailable"
        sportsLabel.text = school.sports ?? "Information Unavailable"
        extraCurrActivityLabel.text = school.extracurricularActivities ?? "Information Unavailable"
    }
    
    
    /// Updates Outlets for SAT Data of School
    ///
    /// - Parameter result: `SATResult` object
    func updateSATLabels(result: SATResult?) {
        guard let result = result else {
            /// Hide SAT outlets if result is nil
            DispatchQueue.main.async {
                self.SATLabels.forEach({ (label) in
                    label.isHidden = true
                })
            }
            
            return
        }
        
        
        DispatchQueue.main.async {
            /// Make outlets Visible
            self.SATLabels.forEach({ (label) in
                label.isHidden = false
            })
            
            /// Update UI with SAT Data of School
            self.SATTakersLabel.text = "SAT Takers\n\(result.takers)"
            self.readingScoreLabel.text = result.criticalReadingAvgScore
            self.writingScoreLabel.text = result.writingAveScore
            self.mathScoreLabel.text = result.mathAvgScore
        }
    }
    
    // MARK: - Helper Methods
    
    
    /// Open default `Maps` app of the iOS
    ///
    /// - Parameters:
    ///   - latitude: `latitude`
    ///   - longitude: `longitude`
    func openMaps(latitude: Double, longitude: Double) {
        let regionDistance:CLLocationDistance = 1000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = school.schoolName
        mapItem.openInMaps(launchOptions: options)
    }
    
    
    /// Downloads SAT Data for selected school using Network Service
    func downloadSATDetail() {
        
        /// We are using `weak` reference to make sure there is no meorry leaks
        NetworkService.SATResult(dbn: school.dbn) { [weak self] (results, error) in
            if let strongSelf = self {
                guard let result = results?.first else {
                    strongSelf.scoreTitleLabel.text = "SAT Details Unavailable"
                    strongSelf.updateSATLabels(result: nil)
                    return
                }
                strongSelf.scoreTitleLabel.text = "Average SAT Scores"
                strongSelf.updateSATLabels(result: result)
            }
        }
    }
    
    
    
    /// Helpful method to display alert using `UIAlertController`
    /// This can also be added in `extension` to easily use in multiple `UIAlertController`
    ///
    /// - Parameters:
    ///   - title: Alert title
    ///   - message: Alert description
    ///   - style: `UIAlertControllerStyle`
    func showAlertWithMessage(title: String?, message: String?, style: UIAlertController.Style = .alert) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
        alertController.addAction(okayAction)
        present(alertController, animated: true, completion: nil)
    }
}

//MARK: - TapGesture Recognizer Setup
extension SchoolDetailViewController {
    
    /// Add tap gesture recognizer on images and Labels to perform specific action based on click
    func addTapGesture() {
        /// `tapGestureAddableImages` contains "UIImageView" collection of which requires to add `UITapGestureRecognizer`
        tapGestureAddableImages.forEach { (imageView) in
            addTapGesture(view: imageView)
        }
        
        /// `tapGestureAddableLabels` contains `UILabel` collection of which requires to add `UITapGestureRecognizer`
        tapGestureAddableLabels.forEach { (label) in
            addTapGesture(view: label)
        }
    }
    
    
    /// Helpful method add tap Gesture on provided view
    /// Single `UITapGestureRecognizer` can only assign to single view.
    /// So we require to add new instance for each view
    ///
    /// - Parameter view: `view` on which `UITapGestureRecognizer` need to be added
    func addTapGesture(view: UIView) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapRecognized(_:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    
    /// Handles tap on view
    /// We get the `view` property of `UITapGestureRecognizer` to differentiate different view and take specific action.
    ///
    /// - Parameter sender: <#sender description#>
    @objc func tapRecognized(_ sender: Any) {
        guard let gestureRecognizer = sender as? UITapGestureRecognizer, let view = gestureRecognizer.view else { return }
        switch view {
        case hoursLabel, hoursImageView:
            print("Hours Clicked")
        case phoneLabel, phoneImageView:
            phoneButtonClicked(self)
        case emailLabel, emailImageView:
            emailButtonClicked(self)
        case websiteLabel, websiteImageView:
            websiteButtonClicked(self)
        case mapLabel, mapImageView:
            mapButtonClicked(self)
        default:
            break
        }
    }
    
}

//MARK: - IBActions
extension SchoolDetailViewController {
    
    
    /// If phone number exists in `School` object and if `UIApplication` can handle Phone Call ask user to make phone call.
    ///
    /// - Parameter sender: `Any`
    @IBAction func phoneButtonClicked(_ sender: Any) {
        guard let url = URL(string: "tel:\(school.phone)"), UIApplication.shared.canOpenURL(url) else {
            showAlertWithMessage(title: "Unable to Call", message: nil)
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    
    
    /// Open `MFMailComposeViewController` controller send email
    ///
    /// - Parameter sender: `Any`
    @IBAction func emailButtonClicked(_ sender: Any) {
        /// Check if sending email is allowed
        guard MFMailComposeViewController.canSendMail() else {
            showAlertWithMessage(title: "Mail services are not available", message: nil)
            return
        }
        
        ///Check if email exists in School object
        guard let email = school.email else {
            showAlertWithMessage(title: "Email Unavailable", message: "Email for this school is not available.")
            return
        }
        
        let composeViewController = MFMailComposeViewController()
        composeViewController.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeViewController.setToRecipients([email])
        composeViewController.setSubject("NYC School Information Request")
        composeViewController.setMessageBody("Hello from School App!", isHTML: false)
        self.present(composeViewController, animated: true, completion: nil)
    }
    
    
    /// Open `SFSafariViewController` with url of school
    ///
    /// - Parameter sender: `Any`
    @IBAction func websiteButtonClicked(_ sender: Any) {
        
        /// Check if school has website
        guard var website = school.website else {
            showAlertWithMessage(title: "Website Unavailable", message: "Website for this school is not available.")
            return
        }
        ///While debugging noticed that there are soome website links where url schemes are missing.
        /// If schemes are missing that add manually to handle missing url schemes
        if !("\(website)".lowercased().hasPrefix("http://")) || !("\(website)".lowercased().hasPrefix("https://")) {
            website = URL(string: "https://".appending("\(website)"))!
        }
        
        /// Finally open `SFSafariViewController` with website of the school.
        let safariController = SFSafariViewController(url: website)
        present(safariController, animated: true, completion: nil)
    }
    
    
    /// Handle the map action to open maps
    ///
    /// - Parameter sender: `Any`
    @IBAction func mapButtonClicked(_ sender: Any) {
        
        guard let latitude = school.latitude, let longitude = school.longitude else {
            showAlertWithMessage(title: "Location Unavailable", message: "Location for this school is not available.")
            return
        }
        
        
        /// Ask user first if they would like to open `Maps`
        let actionController = UIAlertController(title: "Open Maps", message: nil, preferredStyle: .actionSheet)
        let openAction = UIAlertAction(title: "Open", style: .default) { _ in
            self.openMaps(latitude: latitude, longitude: longitude)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        actionController.addAction(openAction)
        actionController.addAction(cancelAction)
        present(actionController, animated: true, completion: nil)
        
    }
}


//MARK: - MFMailComposeViewController Delegate
extension SchoolDetailViewController: MFMailComposeViewControllerDelegate {
    /// Delegate method of `MFMailComposeViewController` to handle completion of sending email or cancel action.
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        ///Dimssing the controller after user is done sending email or on cancel click
        controller.dismiss(animated: true, completion: nil)
    }
}
