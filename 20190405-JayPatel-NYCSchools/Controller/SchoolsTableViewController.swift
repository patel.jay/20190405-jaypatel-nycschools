//
//  SchoolsTableViewController.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController {
    
    var searchController: UISearchController!
    
    /// All schools downloaded from network.
    var schools: [School] = []
    
    /// This are actual schools which are being displayed based on filter string(search controller)
    var visibleSchools: [School] = []
    
    
    /// This filters the visible schools on table view based on search string of `searchController`
    /// We are using property observer to handle any change in `filterString` and reload data
    /// This filters schools and realoads tableview with filtered schools
    /// This also updates `title` property as soon as visible number of schools changes
    var filterString: String? = nil {
        didSet {
            if filterString == nil || filterString!.isEmpty {
                visibleSchools = schools
            }
            else {
                visibleSchools = schools.filter({ (school) -> Bool in
                    school.city.containsIgnoringCase(find: filterString!) || school.schoolName.containsIgnoringCase(find: filterString!)
                })
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.updateNavigationTitle()
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// We are using Dyanimc Type so use automatic dimention to resize cell size based on fonts
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        
        
        setupNavBar()
        addrefreshControl()
        downloadSchools(self)
    }
    
    
    
    /// Dowloads schools data and reloads table view
    ///
    /// - Parameter sender: Triggering Object
    @objc func downloadSchools(_ sender: Any) {
        
        
        
        /// Only use progress hud if download triggered manually (without refresh control of UITableView)
        /// If Triggered using UIRefreshControl refresh control will be shown instead of ProgressHUD
        var progressHUD: MBProgressHUD?
        if !(sender is UIRefreshControl) {
            progressHUD = MBProgressHUD.showAdded(to: self.view, animated: false)
            progressHUD?.removeFromSuperViewOnHide = true
            progressHUD?.label.text = "Downloading Data"
            progressHUD?.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
        
        /// Download School data
        NetworkService.schoolList { (value, error) in
            progressHUD?.hide(animated: true)
            self.refreshControl?.endRefreshing()
            guard let value = value else {
                return
            }
            self.schools = value
            self.visibleSchools = value
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.updateNavigationTitle()
            }
        }
    }
    
    
    /// Set `title` with total number of schools being displayed
    func updateNavigationTitle() {
        title = "\(self.visibleSchools.count) NYC Schools"
    }
    
    
    /// Set up `navigationItem` with `UISearchController` to filter schools by city or name
    /// `iOS 11` has built in search controller in `UINavigationItem`
    /// for earlier than `iOS 11` use `tableHeaderView` property of the UITableView
    func setupNavBar() {
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.tintColor = .white
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search school name or city"
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        
    }
    
    
    /// Refresh Control to reload data for NYC Schools
    func addrefreshControl() {
        let refreshControl = UIRefreshControl()
        let attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "Updating Data", attributes: attributes)
        refreshControl.tintColor = .white
        refreshControl.addTarget(self, action:  #selector(downloadSchools), for: UIControl.Event.valueChanged)
        self.refreshControl = refreshControl
    }
}


// MARK: - TableView data source
extension SchoolsTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visibleSchools.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(SchoolTableViewCell.self)", for: indexPath) as! SchoolTableViewCell
        cell.configure(school: visibleSchools[indexPath.row])
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // Option 1
        /*
         
        let animation = AnimationFactory.makeMoveUpWithBounce(rowHeight: cell.frame.height, duration: 1.0, delayFactor: 0.01)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
         
        */
        
        // Option 2
        // Many more can be added.
        let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.01)
        let animator = Animator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
}


// MARK: - Tableview delegate
extension SchoolsTableViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /// Return Automatic Dimention to Support Dynamic Type
        return UITableView.automaticDimension
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        /// Based on selected school. Grab the school and display details about that school.
        let school = visibleSchools[indexPath.row]
        let schoolDetailController = SchoolDetailViewController.instantiate(fromAppStoryboard: .Main)
        
        ///Pass school object between controllers
        schoolDetailController.school = school
        self.navigationController?.pushViewController(schoolDetailController, animated: true)
    }
}


// MARK: - UISearchResults Updating
extension SchoolsTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard searchController.isActive else {
            return
        }
        filterString = searchController.searchBar.text
    }
}


// MARK: - UISearchBar Delegate
extension SchoolsTableViewController: UISearchBarDelegate {
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        ///Set earlier string if we have search string available for previous searches
        searchBar.text = filterString
        return true
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        /// Clear filters
        /// Reloading data will be handled by property observer
        filterString = nil
    }
}

