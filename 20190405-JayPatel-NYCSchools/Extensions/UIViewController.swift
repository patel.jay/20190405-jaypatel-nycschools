//
//  UIViewController.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import Foundation


/// Helpful enum for easy intialization of `UIViewController`
public enum AppStoryboard : String {
    case Main
    
    
    /// Resturns default instance of `UIStoryboard`
    public var instance : UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    
    /// instantiate viewcontroller and returns the instance of view controller
    ///
    /// - Parameter viewControllerClass: `UIViewController` type
    /// - Returns: `UIViewController` instance
    public func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    
    /// instantiate initial viewcontroller and returns the instance of view controller
    public func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    // Not using static as it wont be possible to override to provide custom storyboardID then
    
    /// Returns strings same as `UIViewController` to use as `storyboardID`
    class var storyboardID : String {
        
        return "\(self)"
        
        // return String(reflecting: self).components(separatedBy: ".").last!
        // return "\(type(of:self))".components(separatedBy: ".").first!
    }
    
    
    /// instantiate viewcontroller and returns the instance of view controller
    ///
    /// - Parameter appStoryboard: `AppStoryboard`
    /// - Returns: `UIViewController` instance
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
