//
//  String.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import Foundation

extension String {
    
    /// Checks if strings contains string provided by `find` attribute
    /// - note: This search is case sensitive
    ///
    /// - Parameter find: string to find
    /// - Returns: `Bool`
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    
    /// Checks if strings contains string provided by `find` attribute
    /// - note: This search is case insensitive
    ///
    /// - Parameter find: string to find
    /// - Returns: `Bool`
    
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
