//
//  RequestRouter.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import Foundation
import Alamofire
/// Request Generator
/// - Note: Conforms to protocol `URLRequestConvertible` of `Alamofire`
///
/// - schoolList: Returns List of NYC Schools
/// - SATResult: SAT Data for specific School
enum RequestRouter: URLRequestConvertible {
    case schoolList
    case SATResult(dbn: String)
    
    
    /// Base URL which is common for all requests
    static let baseURLString =  "https://data.cityofnewyork.us/resource"
    
    
    /// Path based on different request
    var path: String {
        switch self {
        case .schoolList:
            return "/97mf-9njv.json"
            
        case .SATResult(_):
            return "/734v-jeq5.json"
        }
    }
    
    
    /// Query parameters required for request.
    /// - Note: `$$app_token` is common for all requests
    var parameters: Parameters {
        switch self {
        case .schoolList:
            return [
                "$$app_token": Constants.appToken
            ]
            
        case .SATResult(let dbn):
            return [
                "dbn": dbn,
                "$$app_token": Constants.appToken
            ]
        }
    }
    
    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try RequestRouter.baseURLString.asURL()
        let urlRequest = URLRequest(url: url.appendingPathComponent(path))
        return try URLEncoding.default.encode(urlRequest, with: parameters)
    }
    
}
