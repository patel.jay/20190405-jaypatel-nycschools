//
//  NetworkService.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import Foundation
import Alamofire


/// Custom Error for serialzation
/// Capture any underlying Error from the URLSession API
enum BackendError: Error {
    case network(error: Error)
    case dataSerialization(error: Error)
    case jsonSerialization(error: Error)
    case xmlSerialization(error: Error)
    case objectSerialization(reason: String)
    case codableSerialization(error: Error)
}


/// Improtant class for REST Web Services
class NetworkService {
    
    /// Returns list of NYC Schools
    ///
    /// - Parameter completion: completion handler
    public static func schoolList(completion:  @escaping (_ schools: [School]?, _ error: Error?) -> Void) {
        let request = RequestRouter.schoolList
        Alamofire.request(request).validate().responseCodable { (response: DataResponse<[School]>) in
            completion(response.value, response.error)
        }
    }
    
    
    /// Returns SAT Details for specified school
    ///
    /// - Parameters:
    ///   - dbn: `dbn`(id) for school
    ///   - completion: completion handler
    public static func SATResult(dbn: String, completion:  @escaping (_ SATResults: [SATResult]?, _ error: Error?) -> Void) {
        let request = RequestRouter.SATResult(dbn: dbn)
        Alamofire.request(request).validate().responseCodable { (response: DataResponse<[SATResult]>) in
            completion(response.value, response.error)
        }
    }
}
