//
//  School.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import Foundation
import SwiftyJSON

struct School: Codable {
    let dbn: String
    let schoolName: String
    let totalStudents: String
    let overview: String
    let city: String
    let address1: String
    let zip: String
    let state: String
    private let _website: String?
    let phone: String
    let email: String?
    let sports: String?
    let extracurricularActivities: String?
    private let _latitude: String?
    private let _longitude: String?
    let location: String?
    let startTime: String?
    let endTime: String?
    
    var hours: String {
        if startTime == nil && endTime == nil {
            return "Information Unavailable"
        }
        return "\(self.startTime ?? "-") - \(self.endTime ?? "-")"
    }
    
    var websiteString: String {
        guard let website = website else { return "Unavailable" }
        return "\(website)"
    }
    
    var latitude: Double? {
        guard let latitude = _latitude else { return nil }
        return Double(latitude)
    }
    
    var longitude: Double? {
        guard let longitude = _longitude else { return nil }
        return Double(longitude)
    }
    
    var website: URL? {
        guard let website = _website else { return nil }
        return URL(string: website)
    }

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case totalStudents = "total_students"
        case overview = "overview_paragraph"
        case city = "city"
        case address1 = "primary_address_line_1"
        case zip = "zip"
        case state = "state_code"
        case _website = "website"
        case phone = "phone_number"
        case email = "school_email"
        case sports = "school_sports"
        case extracurricularActivities = "extracurricular_activities"
        case _latitude = "latitude"
        case _longitude = "longitude"
        case location = "location"
        case startTime = "start_time"
        case endTime = "end_time"
    }
}
