//
//  SATResult.swift
//  20190405-JayPatel-NYCSchools
//
//  Created by Jay Patel on 4/5/19.
//  Copyright © 2019 Jay Patel. All rights reserved.
//

import Foundation
import SwiftyJSON

struct SATResult: Codable {
    let dbn: String
    let schoolName: String
    let takers: String
    let criticalReadingAvgScore: String
    let mathAvgScore: String
    let writingAveScore: String
    
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case takers = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAveScore = "sat_writing_avg_score"
    }
}
