
# Coding Challenge: NYC Schools
Native app to provide information on NYC High schools.

## Features

- [x] [Data Grabbed From NYC High Schools](https://opendata.cityofnewyork.us)
- [x] Support Dynamic Type
- [x] Display a list of NYC High Schools.
- [x] Filter search results using City or School Name
- [x] Reload data using `UIRefreshControl`
- [x] Custom extension for different components to use across the project.
- [x] Display helpful information after specific school is selected
- [x] Perform based on custom action such as open maps, make a phone call
- [x] Use build it framework such as `SafariServices`,  `MapKit`,  `MessageUI`
- [x] Mix and Match: Objective-C and Swift
- [x] Importing Objective-C into Swift using Objective-C bridging header
- [x] Animation for table reload

